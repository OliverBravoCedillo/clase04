﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clase1 : MonoBehaviour {

    // antes del start se esta montando el objeto
    void Awake () {
        print("AWAKE");
    }

	// Use this for initialization
	void Start () {
        print("START");	
	}
	
	// Update is called once per frame
    // Detección de entradas de usuario
    // movimiento
	void Update () {

        // print("UPDATE");
        // input

        // detectando directo de los dispositivos
        // TRUE - si el cuadro anterior estaba suelta
        // y en el cuadro actual está presionada
        if (Input.GetKeyDown(KeyCode.A)) {
            print("Presionado!");
        }

        // TRUE - cuadro anterior presionado
        // cuadro actual presionado
        if (Input.GetKey(KeyCode.A)) {
            print("KEY");
        }

 
        // TRUE - si el cuadro anterior estaba presionado y en el atual suelto.
        if (Input.GetKeyUp(KeyCode.A)) {
            print("Key Up");
        }

        if (Input.GetMouseButtonDown(0)) {
            print(Input.mousePosition);
        }



        // ejes (esta es la chida)
        // regresa un float valor [-1,1]
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        transform.Translate( h * Time.deltaTime * 5, v * Time.deltaTime * 5 , 0);



    }

    // corre una vez al frame
    // despues del update
    void LateUpdate () {
        // print("LATE UPDATE"); 
    }

    // fijo
    // corre en una frecuencia preconfigurada
    void FixedUpdate () {
        // print("FIXED UPDATE");
    }
}
